import csv

def matchBuddy():
    while(1):
        print("1. Find users based on location.")
        print("2. Find users based on interest.")
        print("3. Exit")
        selection = input("Enter your selection: ")
        if (selection=="1"):
            basedOnLocation()
        elif (selection=="2"):
            basedOnInterest()
        elif (selection=="3"):
            print("Redirecting you to the main menu")
            break
        else:
            print("Please make a valid selection")

def login():
    i = 0
    userID = input("Enter your userID : ")
    for row in database:
        if (row[0]==userID):
            i = 1
            break
    if (i==1):
        matchBuddy()
    else:
        print("No ID matched. Redirecting you to the main menu...")

def join():
    fname = input("Enter your first name: ")
    lname = input("Enter your last name:")
    age = input("Enter your age: ")
    gender = input("Enter your gender:")
    city = input("Enter your city: ")
    job = input("Enter your job:")
    interest = input("Enter your interest: ")
    contact = input("Enter your contact:")
    userID = input("PLEASE PICK A USER ID FOR LOGGING IN NEXT TIME : ")
    try:
        with open(filename, 'a', newline='') as csvfile:
            matchnames = csv.writer(csvfile)
            matchnames.writerow([userID,lname,fname,age,gender,city,job,interest,contact])
        print("\nYou have been added to the database")
    except Exception:
        print("Could not read the file. Please make sure the file name is correct or the file is in the right directory.")

        
    
        


def basedOnLocation():
    location = input("\nEnter the location: ")
    results = []
    for row in database:
        if (row[5].lower()==location.lower()):
            match = "\nName: " + row[2] + " "+ row[1] + "\nAge: " + str(row[3]) + "\nGender: " + row[4] + "\nLocation: " + row[5] + "\nJob: " + row[6] + "\nInterest: " + row[7] + "\nContact Number: " + row[8] + "\n"
            results.append(match)
    if (len(results)!=0):
        print("Here are your matches and their profiles:\n")
        for match in results:
            print(match)
    else:
        print("------------------------------------")
        print ("Sorry! No matches were found for you.\n")



def basedOnInterest():
    interest = input("\nEnter the interest: ")
    results = []
    for row in database:
        if (row[7].lower()==interest.lower()):
            match = "\nName: " + row[2] + " "+ row[1] + "\nAge: " + str(row[3]) + "\nGender: " + row[4] + "\nLocation: " + row[5] + "\nJob: " + row[6] + "\nInterest: " + row[7] + "\nContact Number: " + row[8] + "\n"
            results.append(match)
    if (len(results)!=0):
        print("Here are your matches and their profiles:\n")
        for match in results:
            print(match)
    else:
        print("------------------------------------")
        print ("Sorry! No matches were found for you.\n")


filename = "match.csv"

try:
    with open(filename, 'r') as file:
      reader = csv.reader(file)
      database = list(reader)
    while(1):
        print("==================MENU=====================")
        print ("1. Login by providing your login id")
        print ("2. Join")
        print ("3. Exit")
        option = input("Enter your selection: ")
        if (option == "1"):
            login()
        elif (option == "2"):
            join()
        elif (option == "3"):
            print("Thank you for using our application")
            break
        else:
            print("Please make a valid selection")


except Exception:
    print("Could not read the file. Please make sure the file name is correct or the file is in the right directory.")
